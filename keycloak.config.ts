import { KeycloakService } from 'keycloak-angular';

export function initializeKeycloak(keycloak: KeycloakService): () => any {
    return () =>
      keycloak.init({
        config: {
          url: 'http://localhost:8080/auth',
          realm: 'syncpilot',
          clientId: 'lc-guest-client',
        },
        initOptions: {
          onLoad: 'check-sso', // check-sso passiert via iframe,
          pkceMethod: 'S256',
          silentCheckSsoRedirectUri:
            window.location.origin + '/assets/silent-check-sso.html',
        },
      });
  }
