import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { Base64 } from '../auth/base64';

@Component({
  selector: 'app-secured-page',
  templateUrl: './secured-page.component.html',
  styleUrls: ['./secured-page.component.scss']
})
export class SecuredPageComponent implements OnInit {

  public tokenPayload: Object | null = null;

  constructor(private keyCloakService: KeycloakService) {
  }

  public async ngOnInit(): Promise<void> {
    if (await this.keyCloakService.isLoggedIn()) {
      const token = await this.keyCloakService.getToken();
      const pl = Base64.decode(token.substring(token.indexOf('.') + 1, token.lastIndexOf('.')));
      this.tokenPayload = JSON.stringify(JSON.parse(pl), null, 4);
    }
  }

  public signOut(): void {
    this.keyCloakService.logout(document.location.origin);
  }
}
