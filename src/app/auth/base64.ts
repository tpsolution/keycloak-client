// Typescript migration of base-64@0.1.0
// Although the core implementation is fine, module integration is bad and not compatible with various loaders.
export class Base64
{
    private static readonly TABLE = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
    private static readonly REGEX_SPACE_CHARACTERS = /[\t\n\f\r ]/g;

    static encode(input: string): string {
        input = String(input);
        if (/[^\0-\xFF]/.test(input)) {
            throw Error(
                'The string to be encoded contains characters outside of the ' +
                'Latin1 range.'
            );
        }
        const padding: any = input.length % 3;
        let output = '';
        let position = -1;
        let a: any;
        let b: any;
        let c: any;
        let buffer: any;

        const length = input.length - padding;

        while (++position < length) {
            // tslint:disable-next-line: no-bitwise
            a = input.charCodeAt(position) << 16;
            // tslint:disable-next-line: no-bitwise
            b = input.charCodeAt(++position) << 8;
            c = input.charCodeAt(++position);
            buffer = a + b + c;
            // Turn the 24 bits into four chunks of 6 bits each, and append the
            // matching character for each of them to the output.
            output += (
            // tslint:disable-next-line: no-bitwise
                Base64.TABLE.charAt(buffer >> 18 & 0x3F) +
            // tslint:disable-next-line: no-bitwise
                Base64.TABLE.charAt(buffer >> 12 & 0x3F) +
            // tslint:disable-next-line: no-bitwise
                Base64.TABLE.charAt(buffer >> 6 & 0x3F) +
            // tslint:disable-next-line: no-bitwise
                Base64.TABLE.charAt(buffer & 0x3F)
            );
        }

        if (padding === 2) {
            // tslint:disable-next-line: no-bitwise
            a = input.charCodeAt(position) << 8;
            b = input.charCodeAt(++position);
            buffer = a + b;
            output += (
            // tslint:disable-next-line: no-bitwise
                Base64.TABLE.charAt(buffer >> 10) +
            // tslint:disable-next-line: no-bitwise
                Base64.TABLE.charAt((buffer >> 4) & 0x3F) +
            // tslint:disable-next-line: no-bitwise
                Base64.TABLE.charAt((buffer << 2) & 0x3F) +
                '='
            );
        } else if (padding === 1) {
            buffer = input.charCodeAt(position);
            output += (
            // tslint:disable-next-line: no-bitwise
                Base64.TABLE.charAt(buffer >> 2) +
            // tslint:disable-next-line: no-bitwise
                Base64.TABLE.charAt((buffer << 4) & 0x3F) +
                '=='
            );
        }

        return output;
    }

    static decode(input: string): string {
        input = String(input)
            .replace(Base64.REGEX_SPACE_CHARACTERS, '');
        let length = input.length;
        if (length % 4 === 0) {
            input = input.replace(/==?$/, '');
            length = input.length;
        }
        if (
            length % 4 === 1 ||
            /[^+a-zA-Z0-9/]/.test(input)
        ) {
            throw new Error(
                'Invalid character: the string to be decoded is not correctly encoded.'
            );
        }
        let bitCounter = 0;
        let bitStorage: any;
        let buffer: any;
        let output = '';
        let position = -1;
        while (++position < length) {
            buffer = Base64.TABLE.indexOf(input.charAt(position));
            bitStorage = bitCounter % 4 ? bitStorage * 64 + buffer : buffer;
            // Unless this is the first of a group of 4 characters…
            if (bitCounter++ % 4) {
                // …convert the first 8 bits to a single ASCII character.
                output += String.fromCharCode(
                    // tslint:disable-next-line: no-bitwise
                    0xFF & bitStorage >> (-2 * bitCounter & 6)
                );
            }
        }
        return output;
    }
}
